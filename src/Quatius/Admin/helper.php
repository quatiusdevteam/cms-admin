<?php
 /**
 *	Admin Helper  
 */

if (!function_exists('render_page'))
{
    function render_page($theme, $page, $param=null, $params=[])
    {
        return \Quatius\Admin\Controllers\Content\PagePublicWebController::renderPage($theme, $page, $param, $params);
    }
}

if (!function_exists('render_page_meta'))
{
    function render_page_meta($theme, $page, $param=null, $params=[])
    {
        return \Quatius\Admin\Controllers\Content\PagePublicWebController::renderPageMeta($theme, $page);
    }
}

if (!function_exists('setting'))
{
    function setting($key, $name=null, $default="")
    {
        return app('settings')->get($key, $name, $default);
    }
}

if (!function_exists('setting_set'))
{
    function setting_set($key, $name, $value="", $type="System")
    {
        app('settings')->set($key, $name, $value, $type);
    }
}

if (!function_exists('setting_save'))
{
    function setting_save($key, $name, $value="", $type="System")
    {
        app('settings')->set($key, $name, $value, $type);
    }
}

if (!function_exists('setting_remove'))
{
    function setting_remove($key, $name)
    {
        app('settings')->remove($key, $name);
    }
}

if (!function_exists('setting_unset'))
{
    function setting_unset($key, $name)
    {
        app('settings')->remove($key, $name);
    }
}