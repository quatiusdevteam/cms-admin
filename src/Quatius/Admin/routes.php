<?php

Route::group(['module' => 'Admin', 'namespace' => 'Quatius\Admin\Controllers'], function() {
	Route::get('admin', 'AdminWebController@index');
});	


// Admin routes for module	
Route::group(['prefix' => trans_setlocale() . '/admin/page', 'namespace' => 'Quatius\Admin\Controllers'], function () {
	Route::resource('page', 'Content\PageAdminWebController');
});
	
// User routes for module
/* Route::group(['prefix' => trans_setlocale() . '/user/page'], function () {
	Route::resource('page', 'PageUserWebController');
});
 */
// Public page routes for module
Route::group(['middleware' => ['web'], 'prefix' => trans_setlocale(),'namespace' => 'Quatius\Admin\Controllers'], function () {
    
    if (config('package.page.suffix','.html') != ""){
        Route::get('/{slug}'.config('package.page.suffix','.html').'/{param1?}/{param2?}/{param3?}/{param4?}', 'Content\PagePublicWebController@getPage');
    }
    else {
        try {
			//$routePageList = app('Lavalite\Page\Interfaces\PageRepositoryInterface')->findWhere(config('package.page.filter-active', ['abstract'=>null, 'status'=>1]),['slug'])->pluck('slug')->toArray();
            $middlewarePage = app('page')->groupByMiddleware();
            foreach ($middlewarePage as $middleware=>$routePageList){
                Route::group(['middleware' => $middleware?[$middleware]:[]], function()use($routePageList){
                    foreach ($routePageList as $page){
                        if (array_search($page->slug,config('package.page.reserved_slugs',['', 'admin', 'page']))!==false){
                            continue;
                        }
                        
                        Route::get('/'.$page->slug.'/{param1?}/{param2?}/{param3?}/{param4?}', '\Quatius\Admin\Controllers\Content\PagePublicWebController@getPage')
                            ->defaults('slug',$page->slug);
                    }
                });
            }
        } catch (Exception $e) { // when no database is config.
        }
        
    }
});

// Admin routes for module
Route::group(['prefix' => trans_setlocale() . '/admin/settings', 'namespace' => 'Quatius\Admin\Controllers\Setting'], function () {
	Route::get('setting/0', 'SettingAdminWebController@new');
    Route::resource('setting', 'SettingAdminWebController');
	
	Route::get('cache/clear', 'SettingAdminWebController@flushCache');
});
	
Route::group(['prefix' => trans_setlocale() . '/admin/user', 'namespace' => 'Quatius\Admin\Controllers\User'], function () {
	Route::resource('user', 'UserAdminWebController');
	Route::get('/user/{user}/send-activation', 'UserAdminWebController@sendActivation');
});