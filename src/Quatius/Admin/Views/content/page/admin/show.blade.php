<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {{ trans('page::page.name') }} [{{$page->name}}]</h3>
    <div class="box-tools pull-right">
     	@if (user('admin.web')->hasRole('superuser'))
        	<button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-page' data-href='{{Trans::to('admin/page/page/create')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>
        @endif
        
        @if($page->id)
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-page' data-href='{{ trans_url('/admin/page/page') }}/{{$page->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        	@if (user('admin.web')->hasRole('superuser'))
       		<button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-page' data-datatable='#main-list' data-href='{{ trans_url('/admin/page/page') }}/{{$page->getRouteKey()}}' >
        		<i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
        	</button>
        	@endif
        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
            <li><a href="#settings" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('show-page')
        ->method('PUT')
        ->action(trans_url('admin/page/page/'. $page->getRouteKey()))!!}
        {!!Form::token()!!}
        <div class="tab-content">
            <div class="tab-pane active" id="details">
                {!! Form::text('name')-> disabled()
                -> label(trans(trans('page::page.label.name')))
                -> required()
                -> placeholder(trans(trans('page::page.placeholder.name')))!!}

                {!! Form::textarea('content')
                -> label(trans('page::page.label.content'))
                -> value(e($page['content']))
                -> addClass('html-editor')
                -> placeholder(trans('page::page.placeholder.content'))
                -> disabled()!!}
                
                @include('Component::page-view')
            </div>
            <div class="tab-pane" id="metatags">
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        {!! Form::text('title')-> disabled()
                        -> label(trans('page::page.label.title'))
                        -> placeholder(trans('page::page.placeholder.title'))!!}
                    </div>
                    <div class="col-md-4 col-lg-4">
                        {!! Form::text('heading')-> disabled()
                        -> label(trans('page::page.label.heading'))
                        -> placeholder(trans('page::page.placeholder.heading'))!!}
                    </div>
                    <!-- <div class='col-md-4 col-sm-4'>
                           {!! Form::text('sub_heading')
                           -> label(trans('page::page.label.sub_heading'))
                           -> placeholder(trans('page::page.placeholder.sub_heading'))!!}
                    </div> 
                    <div class='col-md-12 col-sm-12'>
                           {!! Form::textarea('abstract')
                           -> label(trans('page::page.label.abstract'))
                           -> rows(4)
                           -> placeholder(trans('page::page.placeholder.abstract'))!!}
                    </div>
                    <div class='col-md-4 col-sm-6'>
                           {!! Form::text('meta_title')
                           -> label(trans('page::page.label.meta_title'))
                           -> placeholder(trans('page::page.placeholder.meta_title'))!!}
                    </div> -->
                    <div class='col-md-4 col-sm-6'>
                           {!! Form::textarea('keyword')
                           -> label(trans('page::page.label.meta_keyword'))-> disabled()
                           -> placeholder(trans('page::page.placeholder.meta_keyword'))!!}
                    </div>
                    <div class='col-md-4 col-sm-6'>
                           {!! Form::textarea('description')-> disabled()
                           -> label(trans('page::page.label.meta_description'))
                           -> placeholder(trans('page::page.placeholder.meta_description'))!!}
                    </div>
					<div class='col-md-4 col-sm-6'>
                        {!! Form::text('slug')-> disabled()
                        -> label(trans('page::page.label.slug'))
                        -> append(config('package.page.suffix','.html'))
                        -> placeholder(trans('page::page.placeholder.slug'))!!}
                    </div>
					<div class='col-md-4 col-sm-6'>
                        {!! Form::select('view')-> disabled()
                        -> options(config('package.page.views',['page' => 'Default']))
                        -> label(trans('page::page.label.view'))
                        -> placeholder(trans('page::page.placeholder.view'))!!}
					</div>
					<div class='col-md-4 col-sm-6'>
                        {!! Form::hidden('status')
                        -> forceValue('0')!!}
                        {!! Form::checkbox('status')-> value('1')-> disabled()
                        -> label(trans('page::page.label.status'))
                        ->inline()!!}
					</div>
                </div>
            </div>
            @if (user('admin.web')->hasRole('superuser'))
            <div class="tab-pane" id="settings">
                <div class="row">
                    <div class="col-md-6 ">
                        @php
                            $themes = array_merge([""=>"default"], array_combine(array_keys(config('theme.map')),array_keys(config('theme.map'))));
                        @endphp
                        {!!Form::select('theme','Select Theme')->options($themes)-> disabled()!!}
                        
                        {!! Form::text('middleware')-> placeholder("eg. auth:web")-> disabled()!!}

                        {!! Form::hidden('order')!!}
                    </div>
                    <div class='col-md-6'>
                        {!! Form::select('compiler')
                        -> options(trans('page::page.options.compiler'))
                        -> label(trans('page::page.label.compiler'))
                        -> disabled()
                        -> placeholder(trans('page::page.placeholder.compiler'))->value('blade')!!}

                        {!! Form::select('category_id')
                        -> options([])
                        -> label(trans('page::page.label.category'))
                        -> disabled()
                        -> placeholder(trans('page::page.placeholder.category'))!!}
                        
                    </div>
                </div>
            </div>
            @endif
        </div>
    {!!Form::close()!!}
</div>
<div class="box-footer" >
&nbsp;
</div>