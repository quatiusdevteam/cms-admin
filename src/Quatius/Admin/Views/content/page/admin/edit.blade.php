<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.edit') }}  {{ trans('page::page.name') }} [{{$page->name}}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-page'  data-load-to='#entry-page' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-page' data-href='{{Trans::to('admin/page/page')}}/{{$page->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
            @if (user('admin.web')->hasRole('superuser'))
             <li><a href="#settings" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
            @endif
        </ul>
        {!!Form::vertical_open()
        ->id('edit-page')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(Trans::to('admin/page/page/'. $page->getRouteKey()))!!}
        {!!Form::token()!!}
        <div class="tab-content">
            <div class="tab-pane active" id="details">
                {!! Form::text('name')
                -> label(trans(trans('page::page.label.name')))
                -> required()
                -> placeholder(trans(trans('page::page.placeholder.name')))!!}

                {!! Form::textarea('content')
                -> label(trans('page::page.label.content'))
                -> value(e($page['content']))
                -> dataPath('pages/'.$page->slug)
                -> addClass('html-editor')
                -> placeholder(trans('page::page.placeholder.content'))!!}
                
                @include('Component::page-edit')
            </div>
            <div class="tab-pane" id="metatags">
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        {!! Form::text('title')
                        -> label(trans('page::page.label.title'))
                        -> placeholder(trans('page::page.placeholder.title'))!!}
                    </div>
                    <div class="col-md-4 col-lg-4">
                        {!! Form::text('heading')
                        -> label(trans('page::page.label.heading'))
                        -> placeholder(trans('page::page.placeholder.heading'))!!}
                    </div>
                    <!-- <div class='col-md-4 col-sm-4'>
                           {!! Form::text('sub_heading')
                           -> label(trans('page::page.label.sub_heading'))
                           -> placeholder(trans('page::page.placeholder.sub_heading'))!!}
                    </div> 
                    <div class='col-md-12 col-sm-12'>
                           {!! Form::textarea('abstract')
                           -> label(trans('page::page.label.abstract'))
                           -> rows(4)
                           -> placeholder(trans('page::page.placeholder.abstract'))!!}
                    </div>
                    <div class='col-md-4 col-sm-6'>
                           {!! Form::text('meta_title')
                           -> label(trans('page::page.label.meta_title'))
                           -> placeholder(trans('page::page.placeholder.meta_title'))!!}
                    </div> -->
                    <div class='col-md-4 col-sm-6'>
                           {!! Form::textarea('keyword')
                           -> label(trans('page::page.label.meta_keyword'))
                           -> placeholder(trans('page::page.placeholder.meta_keyword'))!!}
                    </div>
                    <div class='col-md-4 col-sm-6'>
                           {!! Form::textarea('description')
                           -> label(trans('page::page.label.meta_description'))
                           -> placeholder(trans('page::page.placeholder.meta_description'))!!}
                    </div>
					<div class='col-md-4 col-sm-6'>
                        {!! Form::text('slug')
                        -> label(trans('page::page.label.slug'))
                        -> append(config('package.page.suffix','.html'))
                        -> placeholder(trans('page::page.placeholder.slug'))!!}
                    </div>
					<div class='col-md-4 col-sm-6'>
                        {!! Form::select('view')
                        -> options(config('package.page.views',['page' => 'Default']))
                        -> label(trans('page::page.label.view'))
                        -> placeholder(trans('page::page.placeholder.view'))!!}
					</div>
					<div class='col-md-4 col-sm-6'>
                        {!! Form::hidden('status')
                        -> forceValue('0')!!}
                        {!! Form::checkbox('status')-> value('1')
                        -> label(trans('page::page.label.status'))
                        ->inline()!!}
					</div>
                </div>
            </div>
            <div class="tab-pane" id="settings">
                <div class="row">
                    <div class="col-md-6 ">
                        @php
                            $themes = array_merge([""=>"default"], array_combine(array_keys(config('theme.map')),array_keys(config('theme.map'))));
                        @endphp
                        {!!Form::select('theme','Select Theme')->options($themes)!!}
                        
                        {!! Form::text('middleware')-> placeholder("eg. auth:web")!!}

                        {!! Form::hidden('order')!!}
                    </div>
                    <div class='col-md-6'>
                        {!! Form::select('compiler')
                        -> options(trans('page::page.options.compiler'))
                        -> label(trans('page::page.label.compiler'))
                        -> placeholder(trans('page::page.placeholder.compiler'))->value('blade')!!}

                        {!! Form::select('category_id')
                        -> options([])
                        -> label(trans('page::page.label.category'))
                        -> placeholder(trans('page::page.placeholder.category'))!!}

                    </div>
                </div>
            </div>
        </div>
         
    @php
    $medias = app("MediaHandler")->getLinkFrom($page);
    @endphp
    
    @foreach($medias as $media)
    	<input type="hidden" class="media_id" data-media_url="{{media_url($media)}}" name="page_medias[][media_id]" value="{{$media->id}}">
    @endforeach
    
    
        {!!Form::close()!!}
    </div>
</div>
<div class="box-footer" >
    &nbsp;
</div>
