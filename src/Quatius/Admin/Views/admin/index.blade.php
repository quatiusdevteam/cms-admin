@includeScript('http://canvasjs.com/assets/script/jquery.canvasjs.min.js')
<style>
a.canvasjs-chart-credit{
	display: none;
}
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
        {!!trans('cms.dashboard')!!}
        <small>{!!trans('cms.version')!!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> {!!trans('cms.home')!!}</a></li>
            <li class="active">{!!trans('cms.dashboard')!!}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box" id="confirmed">
                    <span class="info-box-icon bg-red"><i class="fa fa-trophy"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">CONFIRMED</span>
                        <span class="info-box-number"><small>Qty:</small> <span class='var_quantity'>0</span></span>
                        <span class="info-box-number">$ <span class='var_amount'>0</span></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box" id="pending">
                    <span class="info-box-icon bg-blue"><i class="fa fa-gavel"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Pending</span>
                        <span class="info-box-number"><small>Qty:</small> <span class='var_quantity'>0</span></span>
                        <span class="info-box-number">$ <span class='var_amount'>0</span></span>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box" id="delivered">
                    <span class="info-box-icon bg-purple"><i class="fa fa-truck"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Delivered</span>
                        <span class="info-box-number"><small>Qty:</small> <span class='var_quantity'>0</span></span>
                        <span class="info-box-number">$ <span class='var_amount'>0</span></span>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box" id="cancel">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-recycle"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Cancel</span>
                        <span class="info-box-number"><small>Qty:</small> <span class='var_quantity'>0</span></span>
                        <span class="info-box-number">$ <span class='var_amount'>0</span></span>
                    </div>
                </div>
            </div>                                    
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Top 15 products sold this month</h3>
                        
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin" id="top15order">
                                <thead>
                                    <tr>
                                        <th class="hidden-xs">SKU</th>
                                        <th>Barcode</th>
                                        <th class="hidden-xs">Description</th>
                                        <th>Quantity</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>    
                                <tbody>
                               
                                <tr>
                                	<td class="hidden-xs">ORD22223</td>
                                	<td>100012342</td>
                                	<td class="hidden-xs">Sample 1</td>
                                	<td><span class="pull-right">2</span></td>
                                	<td><span class="pull-right">$123.00</span></td>
                                </tr>
                                <tr>
                                	<td class="hidden-xs">ORD53323</td>
                                	<td>100012342</td>
                                	<td class="hidden-xs">Sample 2</td>
                                	<td><span class="pull-right">2</span></td>
                                	<td><span class="pull-right">$163.00</span></td>
                                </tr>
                                <tr>
                                	<td class="hidden-xs">ORD523323</td>
                                	<td>122012342</td>
                                	<td class="hidden-xs">Sample 3</td>
                                	<td><span class="pull-right">2</span></td>
                                	<td><span class="pull-right">$211.00</span></td>
                                </tr>
                                </tbody>                            
                            </table>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <!-- <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
                        <a href="admin/order" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            	<div class="box box-danger">
            	
            	<h3 class="box-title text-center">Product sold by gender</h3>
            	<div class="info-box bg-red" id="female">
                    <span class="info-box-icon"><i class="fa fa-female"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Women</span>
                        <span class="info-box-number">0</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 20%"></div>
                        </div>
                        <span class="progress-description">
                        20% Increase in 30 Days
                        </span>
                    </div>
                </div>
                <div class="info-box bg-green" id="other">
                    <span class="info-box-icon"><i class="fa fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Unisex</span>
                        <span class="info-box-number">114,381</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        70% Increase in 30 Days
                        </span>
                    </div>
                </div>
                
                <div class="info-box bg-aqua">
                    <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Direct Messages</span>
                        <span class="info-box-number">163,921</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 40%"></div>
                        </div>
                        <span class="progress-description">
                        40% Increase in 30 Days
                        </span>
                    </div>                    
                </div>
                <div class="box box-warning" style="min-height:450px;">                 	                  
                    <div class="box-footer text-center row">
                    	<h3 class="box-title">Product sold by states</h3>
                        <div id="pieChart" ></div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@script
<script>

function drawPieChart(act_amount, nsw_amount, nt_amount, qld_amount, sa_amount, tas_amount, vic_amount, wa_amount){	
	var chart = new CanvasJS.Chart("pieChart", {
	title:{
	text: ""
	},
	animationEnabled: true,
	legend:{
	verticalAlign: "bottom",
	horizontalAlign: "center"
	},
	data: [
	{
	indexLabelFontSize: 14,
	indexLabelFontFamily: "Helvetica",
	indexLabelFontColor: "black",
	indexLabelLineColor: "darkgrey",
	indexLabelPlacement: "outside",
	type: "pie",
	showInLegend: false,
	toolTipContent: "{y} - <strong>#percent%</strong>",
	dataPoints: [
	{ y: vic_amount, legendText:"VIC", exploded: true, indexLabel: "VIC" },
	{ y: nsw_amount, legendText:"NSW", indexLabel: "NSW" },
	{ y: act_amount, legendText:"ACT", indexLabel: "ACT" },
	{ y: nt_amount, legendText:"NT" , indexLabel: "NT"},
	{ y: qld_amount, legendText:"QLD", indexLabel: "QLD" },
	{ y: wa_amount, legendText:"WA" , indexLabel: "WA"},
	{ y: sa_amount, legendText:"SA" , indexLabel: "SA"},
	{ y: tas_amount, legendText:"TAS" , indexLabel: "TAS"}
	]
	}
	]
	});
	chart.render();
	}
</script>
@endscript