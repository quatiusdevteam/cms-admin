
<div class="container">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">					
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<h3 class="title center-text" >SIGN UP</h3>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">			
							@include('public::notifications')													
			                    {!!Form::horizontal_open()			                    
			                    ->id('register')
			                    ->action('register')
			                    ->method('POST')
			                    ->class('white-row')!!}
	                    			                   
	                    		{!! Form::text('first_name', trans('user::user.user.label.first_name'))
	                    		->id('first_name')			                    
			                    -> placeholder(trans('user::user.user.placeholder.first_name'))!!}
			
                    			<br /><br />
                    			{!! Form::email('email')			                    
			                    -> placeholder(trans('user::user.user.placeholder.email'))!!}
			                    
			                    {!! Form::password('password')			                    
			                    -> placeholder(trans('user::user.user.placeholder.password'))!!}
			                    {!! Form::password('password_confirmation')			                    
			                    -> placeholder(trans('user::user.user.placeholder.password_confirmation'))!!}
			                    <div class="col-sm-8 col-sm-offset-4">
			                    {!! Form::hidden(config('user.params.type' ,'r'))->forceValue($guard)!!}
                    			{!! Captcha::render() !!}
                    			<br />
                    			</div>		
                    			<div class="col-xs-12">
                    				<div class="row center-text">
                    					<a class="btn btn-link pull-left" href="{{trans_url("/login")}}">Back to login</a>
                    					{!! Form::submit('Sign up')->class('btn btn-primary pull-right')!!}                    					
                    				</div>
                    			</div>
                    			
                    			{!! Form::close() !!}      
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>