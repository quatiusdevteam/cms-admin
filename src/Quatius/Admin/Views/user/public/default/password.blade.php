<div class="container">
	<div class="col-xs-12 col-sm-6 col-sm-offset-3">
		<div class="row">
			<div class="panel-body panel panel-default">
				<div class="col-xs-12">
					<div class="row">
						<a href="{{trans_url("/login")}}">Back to login</a>
					</div>
					<div class="clearfix"><br /></div>
					<div class="col-xs-12">
						{!!Form::vertical_open()
						->method('POST')
						->action('password/email')!!}
						{!! csrf_field() !!}
						<div class="form-group form-group-lg">
							<label for="email">{{trans('user::user.user.label.email')}}</label>
							<input type="email" class="form-control" id="email" name="email" autocomplete="off" value="{{ old('email') }}" required="required">
						</div>
						<div class="clearfix"></div>
						<div class="form-group form-group-lg">
							@include('public::notifications')
							@if (Session::has('status'))
								<div class="alert alert-info alert-dismissable">
									{!!  Session::pull('status'); !!}
								</div>

							@endif
						</div>

						<div class="form-group form-group-lg">
							<button type="submit" class="btn btn-success btn-md btn-block">Send password</button>
						</div>

						{!! Form::close() !!}
					</div>

				</div>
			</div>
		</div>
	</div>
</div>




<!--

<div class="container">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<h3 class="title center-text" >Forgot Password</h3>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">

								{!!Form::vertical_open()
			                    ->method('POST')
			                    ->action('password/email')!!}
								{!! csrf_field() !!}

								@if (Session::has('status'))
									<div class="alert alert-info">
										<strong>Info!</strong> {!!  Session::pull('status'); !!}
									</div>

								@endif

								{!! Form::email('email','')
                                -> placeholder(trans('user::user.user.placeholder.email'))!!}
								<br />
								{!! Form::submit('Send password')->class('btn btn-primary')!!}
								{!! Form::close() !!}
								<br />
								<div class="col-xs-12">
									<div class="row center-text">
										<a class="btn btn-link" href="{{trans_url("/login?role=$guard")}}">Back to login</a><br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
-->


<!--
<div class="container">
    <div class="row profile">
        <div class="col-md-6  col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Reset <small>Password</small></div>
                <div class="panel-body">
                    {!!Form::vertical_open()
                    ->method('POST')
                    ->action('password/email')!!}
                    {!! csrf_field() !!}
                    @if (Session::has('status'))
                    <div class="alert alert-info">
                        <strong>Info!</strong> {!!  Session::pull('status'); !!}
                    </div>
                    @else
                    If you have forgotten your password - reset it.
                    @endif
                    {!!Form::text('email')!!}
                    <div class="row">

                        <div class="col-xs-6">
                            {!! Form::hidden(config('user.params.type'))!!}
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Send password</button>
                        </div>

                    </div>
                    {!!Form::Close()!!}
                    <br>
                    <a href="{{trans_url("/login?role=$guard")}}">Back to login</a><br>
                </div>
            </div>
        </div>
    </div>
</div>
 -->