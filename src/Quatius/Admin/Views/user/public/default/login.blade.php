@script
<script>
    function showPassword(itm) {
        $("#" +itm).attr('type', "text");
    }
    function hidePassword(itm) {
        $("#" +itm).attr('type', "password");
    }
    $(document).ready(function(){

        $("#showpassword").mousedown(function(){
            showPassword("password");
        });
        $("#showpassword").mouseup(function(){
            hidePassword("password");
        });
    });
</script>
@endscript
<style>
	.input-group-addon {
		padding: 8px 20px;
	}
	#login-extra span{
		display: block;
		text-align: center;
	}
</style>
<div class="container">
	<div class="col-xs-12 col-sm-6 col-sm-offset-3">
		<div class="row">
			<div class="panel-body panel panel-default">
				<div class="col-xs-12">
					{!!Form::vertical_open()
						->id('login')
						->method('POST')
						->action('login')
						->class('white-row')
					!!}
					<div class="form-group form-group-lg">
						<label for="email">{{trans('user::user.user.label.email')}}</label>
						<input type="email" class="form-control" id="email" name="email" autocomplete="off" value="{{ old('email') }}" required="required">
					</div>
					<div class="form-group form-group-lg">
						<label for="password">{{trans('user::user.user.label.password')}}</label>
						<div class="input-group">
							<input type="password" class="form-control" id="password" name="password" required="required">
							<label class="input-group-addon" id="showpassword"><i class="fa fa-eye"></i> </label>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="row">
							<div class="form-check">
								<input type="checkbox" class="form-check-input" name="remember" id="remember" value="1">
								<label class="form-check-label" for="remember">Remember me</label>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="row">
							<div class="pull-right">
								<a href="{{trans_url("/password/reset")}}"> Forget password? </a>
							</div>
						</div>
					</div>
					<div class="clearfix"><br /><br /></div>
					<div class="form-group form-group-lg">
						@include('public::notifications')
					</div>

					<div class="form-group form-group-lg">
						<button type="submit" class="btn btn-success btn-md btn-block">Login</button>
					</div>
					{!! Form::hidden(config('user.params.type'))!!}
					{!! Form::close() !!}
				</div>
			</div>
			<div class="clearfix"><br /></div>
			<div class="panel-body panel panel-default">
				<div class="col-xs-12" id="login-extra">
					<span>Don't have an account?</span>
					<span><a href="{{trans_url("/register")}}">Create one now</a></span>
					<br />
					<span class="small">To enjoy exclusive offers and</span>
					<span class="small">Extended warranty for registered TV</span>
				</div>
			</div>
		</div>
	</div>
</div>

















<!--
<div class="container">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<h3 class="title center-text" >LOGIN</h3>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								@include('public::notifications')
								{!!Form::vertical_open()
			                    ->id('login')
			                    ->method('POST')
			                    ->action('login')
			                    ->class('white-row')!!}

			                    {!! Form::email('email','')
	                    		-> placeholder(trans('user::user.user.placeholder.email'))!!}

	                    		{!! Form::password('password','')
	                    		-> placeholder(trans('user::user.user.placeholder.password'))!!}

	                    		{!! Form::hidden(config('user.params.type'))!!}

                    			<div class="col-xs-12">
                    				<div class="small row">
                    					<input type="checkbox" name="remember" id="remember" value="1"> <label for="remember">Remember me</label>
                    				</div>
                    			</div>
                    			<br />
                    			{!! Form::submit('Login')->class('btn btn-primary')!!}
                    			{!! Form::close() !!}
                    			<br />
                    			<div class="col-xs-12">
                    				<div class="row center-text">
                    					<a class="btn btn-link" href="{{trans_url("/password/reset")}}"> Forget Password? </a>
                    					<br /><br />
                    					<span>Don't have an account? </span> <a class="btn btn-link" href="{{trans_url("/register")}}"> Sign Up now</a>
                    				</div>
                    			</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
-->