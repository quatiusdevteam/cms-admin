<div class="tab-pane active" id="details">
    <div class="row">
        <div class='col-md-3 col-sm-3'>
            <div class='col-md-12 col-sm-12'>
                {!! Form::text('name')
                -> label(trans('user::user.user.label.name'))
                -> placeholder(trans('user::user.user.placeholder.name')) !!}
            </div>
            <div class='col-md-12 col-sm-12'>
                {!! Form::email('email')
                -> label(trans('user::user.user.label.email'))
                -> placeholder(trans('user::user.user.placeholder.email')) !!}
            </div>
            <div class='col-md-12 col-sm-12'>
                {!! Form::password('password')
                -> label(trans('user::user.user.label.password'))
               
                -> placeholder(trans('user::user.user.placeholder.password'))
                
                 -> attributes(["autocomplete"=>"off"]) !!}
            </div>
            @if(user('admin.web')->isSuperuser())
                <div class='col-md-12 col-sm-12'>
                    {!! Form::textarea('params', '')
                    -> attributes(["rows"=>"3"])->class('form-control') !!}
                </div>
            @endif
        </div>
        <div class='col-md-2 col-sm-2'>
            <table class="table">
                <thead>
                    <th>Role</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    @foreach ($roles as $role)
                    <tr>
                        <td>{{ ucfirst($role->name) }}</td>
                        <td>
                            <input name="roles[{{ $role->id }}]" type="checkbox" {{ ( $user-> hasRole($role->name)) ? 'checked' : '' }} value='{{ $role->id }}' >
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class='col-md-7 col-sm-7' style="height:250px;overflow: scroll;">
            <table class="table">
                <thead>
                    <th>Modules</th>
                    <th>Permissions</th>
                </thead>
                <tbody>
                    @foreach($permissions as $keyPermission => $permission)
                    <tr>
                        <td>{{ucfirst($keyPermission)}}</td>
                        <td>
                            @forelse($permission as $key => $val)
                            &nbsp; <input name="permissions[{{ $keyPermission. '.' .$key }}]" type="checkbox" {{ (@array_key_exists($keyPermission. '.' .$key, $user->permissions)) ? 'checked' : '' }} value='1'> {{$val}}
                            @empty
                            No permissions assigned
                            @endforelse
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
{{--
<div class="tab-pane " id="roles">
    <div class="row">
        <div class='col-md-4 col-sm-4'>
            <div class='col-md-12 col-sm-12'>
                {!! Form::radios('sex')
                -> radios(trans('user::user.user.options.sex'))
                -> label(trans('user::user.user.label.sex')) !!}
            </div>
            <div class='col-md-12 col-sm-12'>
				{!! Form::text('first_name')
                -> label(trans('user::user.user.label.first_name'))
                -> placeholder(trans('user::user.user.placeholder.first_name')) !!}
            </div>
            <div class='col-md-12 col-sm-12'>
				{!! Form::text('last_name')
                -> label(trans('user::user.user.label.last_name'))
                -> placeholder(trans('user::user.user.placeholder.last_name')) !!}
            </div>
            
        </div>
        <div class='col-md-4 col-sm-4'>
            <div class='col-md-12 col-sm-12'>
            	{!! Form::select('designation')
                -> options(["User"=>"Public User","b2b"=>"Wholesale B2B","Admin"=>"Administrator"])
                -> label(trans('user::user.user.label.designation'))
                -> placeholder(trans('user::user.user.placeholder.designation')) !!}
            </div>
            <div class='col-md-12 col-sm-12'>
                {!! Form::tel('phone_1')
                -> label(trans('user::user.user.label.phone'))
                -> placeholder(trans('user::user.user.placeholder.phone')) !!}
            </div>
        </div>
        <div class='col-md-4 col-sm-4'>
            <div class='col-md-12 col-sm-12'>
                {!! Form::text('address_1')
                -> label(trans('user::user.user.label.address'))
                -> placeholder(trans('user::user.user.placeholder.address')) !!}
            </div>
            <div class='col-md-12 col-sm-12'>
                {!! Form::text('city')
                -> label(trans('user::user.user.label.city'))
                -> placeholder(trans('user::user.user.placeholder.city')) !!}
            </div>
           <div class='col-md-12 col-sm-12'>
                {!! Form::select('state')
                -> options(trans('user::user.user.options.states'))
                -> label(trans('user::user.user.label.state'))
                -> placeholder(trans('user::user.user.placeholder.state')) !!}
            </div>
            <div class='col-md-6 col-sm-6'>
				{!! Form::select('country')
                -> options(trans('user::user.user.options.countries'))
                -> label(trans('user::user.user.label.country'))
                -> placeholder(trans('user::user.user.placeholder.country')) !!}
            </div>
            <div class='col-md-6 col-sm-6'>
                {!! Form::text('postcode')
                -> label(trans('user::user.user.label.postcode'))
                -> placeholder(trans('user::user.user.placeholder.postcode')) !!}
            </div>
        </div>
        <!-- <div class='col-md-3 col-sm-4'>
            <div class='col-md-12 col-sm-12'>
                {!! Form::file('photo')
                -> label(trans('user::user.user.label.photo'))
                -> placeholder(trans('user::user.user.placeholder.photo')) !!}
            </div>
        </div> -->
    </div>
</div>
--}}
