@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> {!! trans('user::user.user.name') !!} <small> {!! trans('cms.manage') !!} {!! trans('user::user.user.names') !!}</small>
@stop
@section('title')
{!! trans('user::user.user.names') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('user::user.user.names') !!}</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-user'>
</div>
@stop
@section('tools')
{{-- <h4>
<a href='#' class="label label-primary filter-role" data-role=''>All</a>
@foreach(User::roles() as $role)
<a href='#' class="label label-warning filter-role" data-role='{{ $role->name }}'>{{ ucfirst($role->name) }}</a>
@endforeach
</h4> --}}
@stop
@section('content')
<?php 
//set_time_limit(360); // 1 hours

?>
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>updated_at</th>
        <th>ID</th>
        <th>{!! trans('user::user.user.label.name')!!}</th>
        <th>{!! trans('user::user.user.label.email')!!}</th>
        <th>STATUS</th>
        </tr>
    </thead>
    <tbody>
    {{--
         @foreach (\App\User::orderBy('updated_at', 'desc')->limit(1000)->get() as $user)
    <tr data-id="{{hashids_encode($user->id)}}">
        <th>{{$user->updated_at }}</th>
        <th>{{$user->id }}</th>
        <th>{{$user->name }}</th>
        <th>{{$user->email }}</th>
    </tr> 
    @endforeach 
    --}}
    </tbody>
</table>
@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    $('#entry-user').load('{{URL::to('admin/user/user/0')}}');
    oTable = $('#main-list').DataTable({
        "columns": [
            { "data": "updated_at" },
            { "data": "id" },
            { "data": "name" },
            { "data": "email" },
            { "data": "status" }
        ],
        "order": [[ 0, "desc" ]],
        serverSide: true,
        ajax: {
            url: "{{ URL::to('/admin/user/user') }}",
            "dataType": "json",
            "type": "GET",
            "data":{
                extra_fields : [
                    "username","status"
                ],
                hash:{
                    from: "id",
                    to: "eid"
                }
            }
        }
    });
/*

 {
        "ajax": '{{ URL::to('/admin/user/user') }}',
        "columns": [
        { "data": "id" },
        { "data": "name" },
        { "data": "email" },
        { "data": "updated_at" },],
        "userLength": 50
    }
*/

    $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('admin/user/user')}}' + '/' + d.eid);
    });

    $('.filter-role').on( 'click', function (e) {
        role = $( this ).data( "role" );

        oTable.ajax.url('{!! URL::to('/admin/user/user?role=') !!}' + role).load();
        e.preventDefault();
    });
});
</script>
@stop
@section('style')
@stop
