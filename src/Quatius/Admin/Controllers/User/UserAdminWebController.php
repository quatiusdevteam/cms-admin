<?php

namespace Quatius\Admin\Controllers\User;

use App\Http\Controllers\AdminWebController as AdminController;
use App\User;
use Form;
use Litepie\Contracts\User\PermissionRepository;
use Litepie\Contracts\User\RoleRepository;
use Litepie\Contracts\User\UserRepository;
use Litepie\User\Http\Requests\UserAdminWebRequest;
use Illuminate\Support\Str;
use DB;

/**
 * Admin web controller class.
 */
class UserAdminWebController extends AdminController
{
    /**
     * @var Permissions
     */
    protected $permission;

    /**
     * @var roles
     */
    protected $roles;

    /**
     * Initialize user controller.
     *
     * @param type UserRepositoryInterface $user
     *
     * @return type
     */
    public function __construct(UserRepository $user,
        PermissionRepository                       $permission,
        RoleRepository                             $roles) {
        $this->repository = $user;
        $this->permission = $permission;
        $this->roles      = $roles;
        parent::__construct();
    }

    /**
     * Display a list of user.
     *
     * @return Response
     */
    public function index(UserAdminWebRequest $request)
    {
    	$isSuper = user('admin.web')->hasRole('superuser');
        if ($request->wantsJson()) {
            $query = DB::table('users')->whereNull('deleted_at');
            if (!$isSuper)
                $query->whereNotIn('id',[1]);

            $users = dataTableQuery($request->all(), $query);
            return response()->json($users, 200);
        }
        $this->theme->prependTitle(trans('user::user.user.names') . ' :: ');
        return $this->theme->of('Admin::user.admin.user.index')->render();
    }

    /**
     * Display user.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function show(UserAdminWebRequest $request, User $user)
    {
        if (!$user->exists) {
            return response()->view('Admin::user.admin.user.new', compact('user'));
        }

        $permissions = $this->permission->groupedPermissions(true);
        $roles       = $this->roles->all();
        $user->password = "";
        $user->permissions = is_string($user->permissions)?json_decode($user->permissions, true):$user->permissions;
        Form::populate($user);
        return response()->view('Admin::user.admin.user.show', compact('user', 'roles', 'permissions'));
    }
    
 	public function sendActivation(UserAdminWebRequest $request, User $user)
    {
        if ($user->exists) {
        	$authControl = new \App\Http\Controllers\Auth\RegisterController($request);
        	$authControl->sendVerificationMail($user);
        	
            return "success";
        }

       return "failed";
    }
    
    /**
     * Show the form for creating a new user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(UserAdminWebRequest $request)
    {
        $user = $this->repository->newInstance([]);

        Form::populate($user);

        $permissions = $this->permission->groupedPermissions(true);
        $roles       = $this->roles->all();

        return response()->view('Admin::user.admin.user.create', compact('user', 'roles', 'permissions'));

    }

    /**
     * Create new user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(UserAdminWebRequest $request)
    {
        try {
            $attributes            = $request->all();
            $attributes['user_id'] = user_id('admin.web');

            $attributes['status'] = 'Active';
            $attributes['api_token'] = Str::random(60);
            
            $user                  = $this->repository->create($attributes);
            $user->syncRoles($request->get('roles'));
            return response()->json([
                'message'  => trans('messages.success.updated', ['Module' => trans('user::user.name')]),
                'code'     => 204,
                'redirect' => trans_url('/admin/user/user/' . $user->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
             return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }

    }

    /**
     * Show user for editing.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function edit(UserAdminWebRequest $request, User $user)
    {
    	$user->password = "";
        $user->permissions = is_string($user->permissions)?json_decode($user->permissions, true):$user->permissions;
        Form::populate($user);
        $permissions = $this->permission->groupedPermissions(true);
        $roles       = $this->roles->all();
        return response()->view('Admin::user.admin.user.edit', compact('user', 'roles', 'permissions'));
    }

    /**
     * Update the user.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function update(UserAdminWebRequest $request, User $user)
    {
        try {
            $attributes = $request->except(['password', 'roles', 'permissions']);
            $user->permissions = json_encode($request->get('permissions',[]));
            $user->fill($attributes);

            if ($request->get('password',''))
                $user->password = bcrypt($request->get('password',''));
                
            if ($request->has('params'))
                $user->params = $request->get('params','');

            $user->save();
            $user->syncRoles($request->get('roles'));
            return response()->json([
                'message'  => trans('messages.success.updated', ['Module' => trans('user::user.name')]),
                'code'     => 204,
                'redirect' => trans_url('/admin/user/user/' . $user->getRouteKey()),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/user/user/' . $user->getRouteKey()),
            ], 400);

        }

    }

    /**
     * Remove the user.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(UserAdminWebRequest $request, User $user)
    {

        try {

            $t = $user->delete();

            return response()->json([
                'message'  => trans('messages.success.deleted', ['Module' => trans('user::user.name')]),
                'code'     => 202,
                'redirect' => trans_url('/admin/user/user/0'),
            ], 202);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/user/user/' . $user->getRouteKey()),
            ], 400);
        }

    }

}
