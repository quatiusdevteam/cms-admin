<?php

namespace Quatius\Admin\Controllers;

use App\Http\Controllers\AdminWebController as AdminController;
use Illuminate\Http\Request;

/**
 * Admin web controller class.
 */
class AdminWebController extends AdminController
{
    /**
     * Initialize page controller.
     *
     * @param type PageRepositoryInterface $page
     *
     * @return type
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a list of page.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->theme->of('Admin::admin.index')->render();
    }
}
