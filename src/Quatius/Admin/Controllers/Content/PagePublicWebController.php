<?php

namespace Quatius\Admin\Controllers\Content;

use App\Http\Controllers\PublicWebController as PublicController;
use Meta;
use Component;
use Illuminate\Http\Request;
use Event;

class PagePublicWebController extends PublicController
{
    /**
     * Constructor.
     *
     * @param type \Lavalite\Page\Interfaces\PageInterface $page
     *
     * @return type
     */
    public function __construct(\Lavalite\Page\Interfaces\PageRepositoryInterface $page)
    {
        $this->model = $page;
        parent::__construct();
    }
    
    public function getPage(Request $request, $slug, $param1=null, $param2=null, $param3=null, $param4=null)
    {
        $slug = $request->route('slug');
        $param1=$request->route('param1');
        $param2=$request->route('param2');
        $param3=$request->route('param3');
        $param4=$request->route('param4');

        // get page by slug
        //$page = $this->model->getPage($slug);
		$filterData = config('package.page.filter-active', ['abstract'=>null, 'status'=>1]);
        $filterData['slug']=$slug;

        $page = $this->model->findWhere($filterData)->first();

        if (is_null($page)) {
            //abort(404);
            $slug = '404';
            $page = $this->model->getPage($slug);
        }
		
        $params = [];
        $param = $param1;
        if ($param1)
        {
            $params[] = $param1;
        }
        if ($param2)
        {
            $params[] = $param2;
        }
        if ($param3)
        {
            $params[] = $param3;
        }
        if ($param4)
        {
            $params[] = $param4;
        }
        
        if ($page->getParams('theme',''))
            $this->setupTheme(config('theme.map.'.$page->getParams('theme','').'.theme'), config('theme.map.'.$page->getParams('theme','').'.layout'));
        
        return self::renderPage($this->theme, $page, $param, $params);
    }
    
    public static function renderPageMeta($theme, $page)
    {
        if (!Meta::get('title'))
            Meta::set('title', strip_tags($page->getTranslation($page->title)));
        
        if (!Meta::get('description'))
            Meta::set('description', strip_tags($page->getTranslation($page->description)));
        
        if(!$theme->get('keywords'))
            $theme->setKeywords(strip_tags($page->getTranslation($page->keyword)));
        
        if (isset($page->image_path) && !Meta::get('image')){
            Meta::set('image', $page->image_path);
        }
    }
    
    public static function renderPage($theme, $page, $param=null, $params=[])
    {
        Component::renderPositions($page->slug.config('package.page.suffix','.html'), ['page'=>$page, 'param'=>$param, 'params'=>$params]);
        
        self::renderPageMeta($theme, $page);
        // Get view
        $view = $page->view;
        $view = view()->exists('public::page.' . $view) ? $view : 'page';
        
        eventFire('page.render', [$theme, $page, $param=null, $params=[], $view]);
        
        // display page
        if (view()->exists('public::page.' . $view))
            return $theme->of('public::page.' . $view, compact('page','param', 'params'))->render();
            else
                return $theme->of('Admin::content.page.public.page', compact('page','param', 'params'))->render();
                
    }
}
