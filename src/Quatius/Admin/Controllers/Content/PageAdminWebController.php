<?php

namespace Quatius\Admin\Controllers\Content;

use App\Http\Controllers\AdminWebController as AdminController;
use Form;
use Illuminate\Http\Request;
use Lavalite\Page\Interfaces\PageRepositoryInterface;
use Lavalite\Page\Models\Page;
use Illuminate\Database\Eloquent\Collection;
use Component;
use Quatius\Component\Models\ComponentPlacement;
/**
 * Admin web controller class.
 */
class PageAdminWebController extends AdminController
{
    /**
     * Initialize page controller.
     *
     * @param type PageRepositoryInterface $page
     *
     * @return type
     */
    public function __construct(PageRepositoryInterface $page)
    {
        parent::__construct();
        $this->repository = $page;
    }

    /**
     * Display a list of page.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        if ($request->wantsJson()) {
            return $pages = $this->repository->setPresenter('\\Lavalite\\Page\\Repositories\\Presenter\\PageListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->whereNull('abstract')->orWhere('abstract','')->orderBy('id', 'DESC');
                })->skipCache()->all();
            return response()->json($pages, 200);

        }

        $this->theme->prependTitle(trans('page::page.names') . ' :: ');
        return $this->theme->of('Admin::content.page.admin.index')->render();
    }

    /**
     * Display page.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function show(Request $request, Page $page)
    {
        if (!$page->exists) {
            return response()->view('Admin::content.page.admin.new', compact($page));
        }
        
        foreach($page->getParams() as $key => $val){
            if (is_scalar($val))
                $page->setAttribute($key,$val);
        }

        Form::populate($page);

        $placements = Component::getPlacementsByUrl(null, $page);
        
        return response()->view('Admin::content.page.admin.show', compact('page', 'placements'));
    }

    /**
     * Show the form for creating a new page.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $page = $this->repository->newInstance(["status" => 1]);

        Form::populate($page);
        $compRepo = app('ComponentHandler');
        
		if (config('quatius.admin.enable-page-images', true))
		{
		    $compPageImage = $compRepo->findByField('type','page-image')->first();
			if (!$compPageImage)
			    $compPageImage = $compRepo->create(config('quatius.component.types.page-images'));
			
			$placement = new ComponentPlacement(['position'=>config('quatius.component.types.'.$compPageImage->type.'.position')]);
			
			$placement->component_id = $compPageImage->id;
			 
			$placements = Collection::make([$placement]);
        }
		else
			$placements = Collection::make([]);
        
        $selectableTypes = [];
        foreach (config('quatius.component.types') as $type=>$component)
        {
        	if($component['selectable'])
        		$selectableTypes[$type] = $component['name'];
        }
        
        $componentList = [];
        return response()->view('Admin::content.page.admin.create', compact('page', 'placements','componentList','selectableTypes'));
    }

    /**
     * Create new page.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            
            if (array_search($request->get("slug",""),config('package.page.reserved_slugs',[]))!==false) {
                throw new \Exception('Invalid Slug');
            }
            
            $attributes            = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            $middleware     =$attributes['middleware'];
            unset($attributes['middleware']);
            
            $page = $this->repository->create($attributes);
            if ($middleware){
                $page->setParams('middleware', $middleware);
                $page->save();
            }
            $componentController = App('Quatius\Component\Controllers\ComponentController');
            $componentController->saveComponents($request, null, $page);
            
            return response()->json(
                [
                    'message'  => trans('messages.success.updated', ['Module' => trans('page::page.name')]),
                    'code'     => 204,
                    'redirect' => trans_url('/admin/page/page/' . $page->getRouteKey()),
                ],
                201);

        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }

    }

    /**
     * Show page for editing.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function edit(Request $request, Page $page)
    {
        foreach($page->getParams() as $key => $val){
            if (is_scalar($val))
                $page->setAttribute($key,$val);
        }

        Form::populate($page);
        
        //$placements = Component::getPlacementsByUrl($page->slug.config('package.page.suffix','.html'));
        $placements = Component::getPlacementsByUrl(null, $page, false);
        
    	$compRepo = app('ComponentHandler');
    	
    	if (config('quatius.admin.enable-page-images', true) && $placements->where('component.type','page-images')->count() == 0)
    	{
    	    $compPageImage = $compRepo->findByField('type','page-image')->first();
    	    if (!$compPageImage)
    	        $compPageImage = $compRepo->create(config('quatius.component.types.page-images'));
    	        
            $placement = new ComponentPlacement(['position'=>config('quatius.component.types.'.$compPageImage->type.'.position')]);
            
            $placement->component_id = $compPageImage->id;
            $placements->prepend($placement);
        }
        
    	$user = user('admin.web');
	    $selectableTypes = [];
	    foreach (config('quatius.component.types') as $type=>$component)
	    {
	        if($component['selectable'] && ($user->hasRole('superuser') || $user->hasPermission($component['detachable_by'])))
	            $selectableTypes[$type] = $component['name'];
        }
        
	    $componentList = $compRepo->findWhereIn('type',array_keys($selectableTypes), ['name','id'])->pluck('name','id');
	    
        return response()->view('Admin::content.page.admin.edit', compact('page', 'placements','componentList','selectableTypes'));
    }

    /**
     * Update the page.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function update(Request $request, Page $page)
    {
       	try {
       	    if (array_search($request->get("slug",""),config('package.page.reserved_slugs', []))!==false) {
       	        throw new \Exception('Invalid Slug');
       	    }
       	    
       	    $attributes = $request->except(['theme', 'middleware']);
               
       	    $medias = $request->get('page_medias',[]);
       	    app("MediaHandler")->updateLink($medias, $page);
            $this->repository->update($attributes, $page->id);
            
            if ($request->get('middleware',''))
                $page->setParams('middleware', $request->get('middleware',''));
            else
                $page->unsetParams('middleware');

            if ($request->get('theme',''))
                $page->setParams('theme', $request->get('theme',''));
            else
                $page->unsetParams('theme');

            $page->save();

            $componentController = App('Quatius\Component\Controllers\ComponentController');
            $componentController->saveComponents($request, null, $page);
            
            return response()->json(
                [
                    'message'  => trans('messages.success.updated', ['Module' => trans('page::page.name')]),
                    'code'     => 204,
                    'redirect' => trans_url('/admin/page/page/' . $page->getRouteKey()),
                ],
                201);

        } catch (Exception $e) {

            return response()->json(
                [
                    'message'  => $e->getMessage(),
                    'code'     => 400,
                    'redirect' => trans_url('/admin/page/page/' . $page->getRouteKey()),
                ],
                400);

        }
 	
    }

    /**
     * Remove the page.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(Request $request, Page $page)
    {

        try {
            app("MediaHandler")->deleteLinkFrom($page);
            
            $this->repository->delete($page->id);
            
            return response()->json(
                [
                    'message'  => trans('messages.success.deleted', ['Module' => trans('page::page.name')]),
                    'code'     => 202,
                    'redirect' => trans_url('/admin/page/page/0'),
                ],
                202);

        } catch (Exception $e) {

            return response()->json(
                [
                    'message'  => $e->getMessage(),
                    'code'     => 400,
                    'redirect' => trans_url('/admin/page/page/' . $page->getRouteKey()),
                ],
                400);
        }

    }

}
