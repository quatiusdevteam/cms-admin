<?php 

namespace Quatius\Admin\Helpers;

use Lavalite\Settings\Models\Setting;
use View;
use Cache;
/**
 *
 */
class Settings
{

    protected $model;

    public function __construct(\Lavalite\Settings\Interfaces\SettingRepositoryInterface $settings)
    {
        $this->model = $settings;
    }

       /**
     * Calls page repository function.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->model->$name($arguments[0]);
    }

    /**
     * Display settings of the user
     *
     * @return Response
     */
    public function display($view)
    {
        return view('settings::admin.setting.'.$view);
    }

    public function count(array $filters = null)
    {
        return  $this->model->count();
    }
    
	public function getCacheId($key, $name=null)
	{
		if ($name)
			return 'setting_'.$key.'_'.$name;
		
		return 'setting_'.$key.'_';
	}
	
    public function get($key, $name=null, $default="")
    {
    	if ($name==null)
    	{
    		return Cache::rememberForever($this->getCacheId($key, $name), function () use($key){
    			return Setting::whereSkey($key)->pluck('value','name')->toArray();
    		});
    	}
    	
        $setting = Cache::rememberForever($this->getCacheId($key, $name), function () use($key, $name){
        	return Setting::whereName($name)->whereSkey($key)->firstOrNew([]);
        });
        
        if (is_int($default)){
            return $setting->exists?intVal($setting->value):$default;
        }
        elseif (is_bool($default)){
            return $setting->exists?boolVal($setting->value):$default;
        }
        elseif (is_float($default)){
            return $setting->exists?floatVal($setting->value):$default;
        }
        
        return $setting->exists?$setting->value:$default;
    }
    
    public function setArray($key, $values=[], $type="System")
    {
    	foreach ($values as $name=>$value)
    	{
    		$this->set($key, $name, $value, $type);
    	}
    }
    
    public function set($key, $name, $value="", $type="System")
    {
    	Cache::forget($this->getCacheId($key, null)); // remove array keys too.
    	Cache::forget($this->getCacheId($key, $name));
    	
    	if (is_array($name))
    		return $this->setArray($key, $name, $type);
    	
    	$setting = Setting::whereName($name)->whereSkey($key)->first();
    	
    	$attributes = [];
    	$attributes['skey'] = $key;
    	$attributes['name'] = $name;
    	$attributes['value'] = $value;
    	$attributes['type'] = $type;
    	
    	if ($setting)
    		$setting->update($attributes);
    	else 
    		Setting::create($attributes);
    }
    
    public function remove($key, $name=null)
    {
        Cache::forget($this->getCacheId($key, null)); // remove array keys too.
        
    	if ($name == null)
    		return Setting::whereSkey($key)->forceDelete();
    	else {
            Cache::forget($this->getCacheId($key, $name));
            return Setting::whereName($name)->whereSkey($key)->forceDelete();
        }
    }
}
