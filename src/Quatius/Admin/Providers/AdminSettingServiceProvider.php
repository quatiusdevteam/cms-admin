<?php

namespace Quatius\Admin\Providers;

use Lavalite\Settings\Providers\SettingsServiceProvider;


class AdminSettingServiceProvider extends SettingsServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    	$this->app->bind('settings', function ($app) {
    		return $this->app->make('Quatius\Admin\Helpers\Settings');
    	});
    	
   		$this->app->bind(
    			\Lavalite\Settings\Interfaces\SettingRepositoryInterface::class,
    			\Lavalite\Settings\Repositories\Eloquent\SettingRepository::class
    	);
        $this->app->register(\Lavalite\Settings\Providers\AuthServiceProvider::class);
    }
}
