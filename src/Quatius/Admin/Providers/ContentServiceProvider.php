<?php

namespace Quatius\Admin\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Request;
use Route;
use Quatius\Admin\Overrides\CustomGuestGateway;
use Lavalite\Settings\Models\Setting;

class ContentServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot()
    {
		parent::boot();
		
		if (Request::is('*/page/page/*')) {
		    Route::bind('page', function ($id) {
				return $this->app->make('page')->findOrNew($id);
			});
		}
		
		if (Request::is('*admin/settings/*')) {
			
		    Route::bind('setting', function ($id) {
				if (!preg_match("/\D/",$id))
					return Setting::find($id);
                    
				return $this->app->make('settings')->findOrNew($id);
			});
		}
		$this->loadTranslationsFrom(__DIR__.'/../Translations', 'page');
		
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
    	return ['admin'];
    }
    
    /**
     * Register the service provider.
     */
    public function register()
    {
    	$this->app->bind('page', function ($app) {
    		return $this->app->make('Lavalite\Page\Page');
    	});
    
    	$this->app->bind(
    			\Lavalite\Page\Interfaces\PageRepositoryInterface::class,
    			\Lavalite\Page\Repositories\Eloquent\PageRepository::class
    	);
    	
    	/*
    	 * Lavalite Framework Service Providers...
    	*/
    	$this->app->register(\Litepie\Form\FormServiceProvider::class);
    	$this->app->register(\Litepie\Trans\TransServiceProvider::class);
    	$this->app->register(\Litepie\Filer\FilerServiceProvider::class);
    	$this->app->register(\Litepie\Hashids\HashidsServiceProvider::class);
    	$this->app->register(\Litepie\User\UserServiceProvider::class);
    	$this->app->register(\Litepie\Menu\MenuServiceProvider::class);
    	 
    	$this->app->register(\Lavalite\Page\Providers\AuthServiceProvider::class);
    	$this->app->register(\Eusonlito\LaravelMeta\MetaServiceProvider::class);
    	
    	$loader = \Illuminate\Foundation\AliasLoader::getInstance();
    	$loader->alias('Captcha', 'Litepie\Support\Facades\Captcha');
    	$loader->alias('Form', 'Litepie\Support\Facades\Form');
    	$loader->alias('Filer', 'Litepie\Support\Facades\Filer');
    	$loader->alias('Hashids', 'Litepie\Support\Facades\Hashids');
    	$loader->alias('Menu', 'Litepie\Support\Facades\Menu');
    	$loader->alias('Trans', 'Litepie\Support\Facades\Trans');
    	$loader->alias('User', 'Litepie\Support\Facades\User');
    	$loader->alias('Page', 'Lavalite\Page\Facades\Page');
    	$loader->alias('Settings', 'Lavalite\Settings\Facades\Settings');
    	$loader->alias('Task', 'Lavalite\Task\Facades\Task');
    	$loader->alias('Calendar', 'Lavalite\Calendar\Facades\Calendar');
    	$loader->alias('Message', 'Lavalite\Message\Facades\Message');
    	$loader->alias('Package', 'Lavalite\Package\Facades\Package');
    	
    	$loader->alias('Meta', 'Eusonlito\LaravelMeta\Facade');
    	
    	$this->app->singleton(\Illuminate\Contracts\Auth\Access\Gate::class, function ($app) {
    		return new CustomGuestGateway($app, function () use ($app) {
    			return $app['auth']->user();
    		});
    	});
    	
    	
    }
    public function deferredProviders(){
    	return [
	    	//\Litepie\User\Providers\RouteServiceProvider::class,
    	];
    }
    public function map()
    {
    }
}
