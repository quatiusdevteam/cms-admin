<?php

namespace Quatius\Admin\Overrides;

use Auth;
use Cache;

class CustomGuestGateway extends \Illuminate\Auth\Access\Gate
{
	/**
	 * Determine if the given ability should be granted for the current user.
	 *
	 * @param  string  $ability
	 * @param  array|mixed  $arguments
	 * @return bool
	 */
	public function check($ability, $arguments = [])
	{
		if (Auth::guest())
		{
			if (function_exists('guest_user')) {
				if (guest_user()->hasPermission($ability))
					return true;
			}
			else 
			{
			    $guestPermissions = Cache::rememberForever("CustomGuestGateway-".config('quatius.admin.guest-role', 'guest')."-permissions",function (){
			        return \Litepie\User\Models\Role::whereName(config('quatius.admin.guest-role', 'guest'))->first()->permissions;
			    });
			    
				if (isset($guestPermissions[$ability]))
				{
					if ($guestPermissions[$ability] == 1)
						return true;
				}
			}
		}
		else {
			if (Auth::user()->hasPermission($ability))
				return true;
		}
		return parent::check($ability, $arguments);
	}
}